var express = require('express');
var mongoose   = require('mongoose');

var uri = 'mongodb://localhost/test';
global.db = mongoose.createConnection(uri);

var routes = require('./routes');

var app = express();
app.set('view engine', 'ejs');
app.get('/', routes.home);
app.get('/insert', routes.insert);
app.get('/name', routes.modelName);
//以下テスト
app.get('/test', routes.testRedirect);

app.post('/', function (req, res) {
  res.json()
});

app.listen(8000, function() {
  console.log('listening on http://localhost:8000');
});
