/* GET helo page. */
router.get('/helo', function(req, res, next) {
    var p1 = req.query["p1"];
    var p2 = req.query.p2;
    var msg = p1 == undefined ? "" : p1 + "," + p2;
    res.render('helo',
        {
            title: 'HELO Page',
            msg: msg,
            input: ''
        }
    );
});

/* POST helo page. */
router.post('/helo', function(req, res, next) {
    var str = req.query.input1;
    res.json(
        { msg: str }
    );
});

module.exports = router;
