var model = require('./modelA');

exports.home = function(req, res, next) {
  model.find(function(err, docs) {
    if (err) return next(err);
    //res.send(docs);
    res.render('index', {
      name: docs
    })
  });
};

exports.modelName = function(req, res) {
  res.send('my model name is ' + model.modelName);
};

exports.insert = function(req, res, next) {
  model.create({name: 'inserting ' + Date.now()}, function(err, doc) {
    if (err) return next(err);
    res.send(doc);
  });
};

//以下テスト
exports.testRedirect = function(req, res, next) {
  console.log('リダイレクト')
  res.redirect('/foo/bar');
}
